package tasks.model.validator;

import tasks.model.Task;

public class TaskValidator {
    public TaskValidator() {}

    public void validate(Task task) throws IllegalArgumentException {
        if (task.isRepeated()) {
            validateRepeatableTask(task);

            return;
        }

        validateNonRepeatableTask(task);
    }

    private void validateNonRepeatableTask(Task task) throws IllegalArgumentException {
        StringBuilder err = new StringBuilder();

        if (null == task.getTitle() || task.getTitle().length()<5 || task.getTitle().length()>50) {
            err.append("Title should have between 5 and 50 characters.\n");
        }
        if (task.getStartTime().getTime() < 0) {
            err.append("Time cannot be negative.\n");
        }

        if (err.toString().length() != 0) { // StringBuild.isEmpty() requires language level > 15
            throw new IllegalArgumentException(err.toString());
        }
    }

    private void validateRepeatableTask(Task task) throws IllegalArgumentException {
        StringBuilder err = new StringBuilder();

        if (null == task.getTitle() || task.getTitle().length()<5 || task.getTitle().length()>50)
            err.append("Title should have between 5 and 50 characters.\n");
        if (task.getStartTime().after(task.getEndTime())) {
            err.append("Start date should be before end date.\n");
        }
        if (task.getStartTime().getTime() < 0 || task.getEndTime().getTime() < 0) {
            err.append("Time cannot be negative.\n");
        }
        if (task.getRepeatInterval() < 1) {
            err.append("Interval should be >= 1.\n");
        }

        if (err.toString().length() != 0) {
            throw new IllegalArgumentException(err.toString());
        }
    }
}
