package tasks.services;

import tasks.model.Task;

import java.util.Date;

public class TaskFactory {

    public static Task createNonRepeatableTask(String title, Date startDate, boolean isActive) {
        Task task = new Task(title, startDate);
        task.setActive(isActive);

        return task;
    }

    public static Task createRepeatableTask(String title, Date startDate, Date endDate, int interval, boolean isActive) {
        Task task = new Task(title, startDate, endDate, interval);
        task.setActive(isActive);

        return task;
    }
}
