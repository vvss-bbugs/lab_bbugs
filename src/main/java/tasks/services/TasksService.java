package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
import tasks.controller.Controller;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.model.validator.TaskValidator;

import java.util.ArrayList;
import java.util.Date;

public class TasksService {

    private ArrayTaskList tasks;
    private static final Logger log = Logger.getLogger(TasksService.class.getName());
    private TaskValidator taskValidator;

    public TasksService(ArrayTaskList tasks, TaskValidator taskValidator){
        this.tasks = tasks;
        this.taskValidator = taskValidator;
    }

    public ObservableList<Task> getObservableList(){
        return FXCollections.observableArrayList(tasks.getAll());
    }

    public Iterable<Task> filterTasks(ObservableList<Task> tasks, Date start, Date end){
        log.info("TaskService: filterTasks() called with parameters start=" +start+" end="+end);
        ArrayList<Task> incomingTasks = new ArrayList<>();
        if (end.before(start))
            return incomingTasks;
        for (int i=0; i<tasks.size(); i++) {
            Task currentTask = tasks.get(i);
            Date nextTime = currentTask.nextTimeAfter(start);
            if (nextTime != null) {
                if (nextTime.before(end) || nextTime.equals(end)) {
                    incomingTasks.add(currentTask);
                    log.info(currentTask.getTitle() + "with start=" + currentTask.getFormattedDateStart() + "selected");
                }
            }
        }
        return incomingTasks;
    }

    /**
     * Adds a new task
     * @param task - the Task to be saved
     * @param taskList - an ObservableList<Task> used by application views
     * @return the added task if task's fields are valid and adds the added task to taskList
     * @throws IllegalArgumentException if task is not valid
     * for a valid task (
     *      title should have between 5 and 50 characters,
     *      startDate should be before endDate for repeatable tasks
     *      interval should be >= 1 for repeatable tasks
     *      )
     */
    public Task add(Task task, ObservableList<Task> taskList) throws IllegalArgumentException {
        taskValidator.validate(task);

        taskList.add(task);
        TaskIO.rewriteFile(taskList);

        return task;
    }

    /**
     * Updates the task at a given index in a taskList
     * @param index - index of the task in taskList
     * @param task - the Task to be updated
     * @param taskList -  an ObservableList<Task> used by application views
     * @return the updated task if task's fields are valid and sets that task to taskList at the given index
     * @throws IllegalArgumentException if task is not valid
     * for a valid task (
     *      title should have between 5 and 50 characters,
     *      startDate should be before endDate for repeatable tasks
     *      interval should be >= 1 for repeatable tasks
     *      )
     */
    public Task edit(int index, Task task, ObservableList<Task> taskList) throws IllegalArgumentException {
        taskValidator.validate(task);

        taskList.set(index, task);
        TaskIO.rewriteFile(taskList);

        return task;
    }
}
