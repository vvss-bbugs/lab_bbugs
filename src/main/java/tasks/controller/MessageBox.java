package tasks.controller;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;

public class MessageBox {
    public static void showErrorMessage(String header, String message){
        showAlert(AlertType.ERROR, "Error", header, message);
    }

    public static void showWarningMessage(String header, String message){
        showAlert(AlertType.WARNING, "Warning", header, message);
    }

    public static void showInformationMessage(String header, String message){
        showAlert(AlertType.INFORMATION,"Information", header, message);
    }

    private static void showAlert(AlertType type, String title,String header, String message){
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
