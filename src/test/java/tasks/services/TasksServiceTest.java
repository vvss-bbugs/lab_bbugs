package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.model.validator.TaskValidator;

import java.util.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TasksServiceTest {
    private TasksService tasksService;
    private ObservableList<Task> taskList;

    @BeforeEach
    void setUp() {
        tasksService = new TasksService(new ArrayTaskList(), new TaskValidator());
        taskList = FXCollections.observableList(new ArrayList<>());
    }

    @Test
    @Tag("ECA")
    @DisplayName("TC01_EC")
    void add_ValidTask_ReturnsAddedTask() {
        int taskListSizeBefore = taskList.size();
        Task validTask = new Task(
                "titlu de lungime valida",
                new Date(2022, Calendar.MARCH, 13, 17, 0),
                new Date(2022, Calendar.MARCH, 13, 18, 0),
                120
        );

        Task returnedTask = tasksService.add(validTask, taskList);

        assertEquals(returnedTask, validTask);
        assertEquals(taskList.size(), taskListSizeBefore + 1);
    }

    @Test
    @Tag("ECA")
    @DisplayName("TC02_EC")
    void add_TitleTooShortInvalidInterval_ThrowsIllegalArgumentException() {
        int taskListSizeBefore = taskList.size();
        Task invalidTask = new Task(
            "sub",
            new Date(2022, Calendar.MARCH, 13, 17, 0),
            new Date(2022, Calendar.MARCH, 13, 18, 0),
            0
        );

        assertThrows(IllegalArgumentException.class, () -> tasksService.add(
                invalidTask,
                taskList
        ));

        assertEquals(taskList.size(), taskListSizeBefore);
    }

    @Test
    @Tag("ECA")
    @DisplayName("TC03_EC")
    void add_TitleTooLong_ThrowsIllegalArgumentException() {
        int taskListSizeBefore = taskList.size();
        Task invalidTask = new Task(
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            new Date(2022, Calendar.MARCH, 13, 17, 0),
            new Date(2022, Calendar.MARCH, 13, 18, 0),
            180
        );

        assertThrows(IllegalArgumentException.class, () -> tasksService.add(
            invalidTask,
            taskList
        ));

        assertEquals(taskList.size(), taskListSizeBefore);
    }

    @Test
    @Tag("BVA")
    @DisplayName("TC01_BVA")
    void add_TaskTitleTooShort_ThrowsIllegalArgumentException() {
        int taskListSizeBefore = taskList.size();
        Task invalidTask = new Task(
            "abcd",
            new Date(2022, Calendar.MARCH, 13, 17, 0),
            new Date(2022, Calendar.MARCH, 13, 18, 0),
            120
        );

        assertThrows(IllegalArgumentException.class, () -> tasksService.add(
            invalidTask,
            taskList
        ));

        assertEquals(taskList.size(), taskListSizeBefore);
    }

    @Test
    @Tag("BVA")
    @DisplayName("TC02_BVA")
    void add_ValidTaskTitleUpperLimits_ReturnsAddedTask() {
        int taskListSizeBefore = taskList.size();
        Task validTask = new Task(
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                new Date(2022, Calendar.MARCH, 13, 17, 0),
                new Date(2022, Calendar.MARCH, 13, 18, 0),
                120
        );

        Task returnedTask = tasksService.add(validTask, taskList);

        assertEquals(returnedTask, validTask);
        assertEquals(taskList.size(), taskListSizeBefore + 1);
    }

    @ParameterizedTest
    @MethodSource("getTitleUpperLimitsTestData")
    @Tag("BVA")
    @DisplayName("TC02-05_BVA")
    void add_ValidTaskTitleSourcedUpperLimits_ReturnsAddedTask(String title) {
        int taskListSizeBefore = taskList.size();
        Task validTask = new Task(
            title,
            new Date(2022, Calendar.MARCH, 13, 17, 0),
            new Date(2022, Calendar.MARCH, 13, 18, 0),
            120
        );

        Task returnedTask = tasksService.add(validTask, taskList);

        assertEquals(returnedTask, validTask);
        assertEquals(taskList.size(), taskListSizeBefore + 1);
    }

    @Test
    @Tag("BVA")
    @DisplayName("TC03_BVA")
    void add_TaskIntervalTooLow_ThrowsIllegalArgumentException() {
        int taskListSizeBefore = taskList.size();
        Task invalidTask = new Task(
            "titlu valid",
            new Date(2022, Calendar.MARCH, 13, 17, 0),
            new Date(2022, Calendar.MARCH, 13, 18, 0),
            0
        );

        assertThrows(IllegalArgumentException.class, () -> tasksService.add(
            invalidTask,
            taskList
        ));

        assertEquals(taskList.size(), taskListSizeBefore);
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2})
    @Tag("BVA")
    @DisplayName("TC04-06_BVA")
    void add_ValidTaskIntervalLowerLimits_ReturnsAddedTask(int interval) {
        int taskListSizeBefore = taskList.size();
        Task validTask = new Task(
            "titlu valid",
            new Date(2022, Calendar.MARCH, 13, 17, 0),
            new Date(2022, Calendar.MARCH, 13, 18, 0),
            interval
        );

        Task returnedTask = tasksService.add(validTask, taskList);

        assertEquals(returnedTask, validTask);
        assertEquals(taskList.size(), taskListSizeBefore + 1);
    }

    /**
     * @return a list containing 2 strings with fixed lengths of 49 and 50
     */
    static Collection<String> getTitleUpperLimitsTestData() {
        StringBuilder title = new StringBuilder();
        for (int i = 0; i < 49; i++) {
            title.append('a');
        }

        return Arrays.asList(
                title.toString(),
                title.append('a').toString()
        );
    }

    @Test
    @DisplayName("F02_TC01")
    void filterTasks_EndBeforeStart_ReturnsEmptyList(){
        Iterable<Task> expectedResult = new ArrayList<>();
        Iterable<Task> actualResult = tasksService.filterTasks(
                taskList,
                new Date(2022, Calendar.APRIL, 14, 17, 0),
                new Date(2022, Calendar.MARCH, 14, 17, 0)
        );

        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("F02_TC02")
    void filterTasks_EmptyList_ReturnsEmptyList() {
        Iterable<Task> expectedResult = new ArrayList<>();
        Iterable<Task> actualResult = tasksService.filterTasks(
                taskList,
                new Date(2022, Calendar.MARCH, 14, 17, 0),
                new Date(2022, Calendar.APRIL, 14, 17, 0)
        );

        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("F02_TC03")
    void filterTasks_ListContainingTaskExecutingBeforeStart_ReturnsEmptyList(){
        Task task = new Task(
                "task executing before start",
                new Date(2022, Calendar.MARCH, 16, 17, 0)
        );
        task.setActive(true);
        taskList.add(task);

        Iterable<Task> expectedResult = new ArrayList<>();
        Iterable<Task> actualResult = tasksService.filterTasks(
                taskList,
                new Date(2022, Calendar.APRIL, 14, 17, 0),
                new Date(2022, Calendar.MAY, 14, 17, 0)
        );

        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("F02_TC04")
    void filterTasks_ListContainingTaskBetweenStartAndEnd_ReturnsListContainingTheMatchingTask() {
        Task task = new Task(
                "task executing between start and end",
                new Date(2022, Calendar.MARCH, 3, 19, 0)
        );
        task.setActive(true);
        taskList.add(task);

        Iterable<Task> expectedResult = new ArrayList<>(Collections.singletonList(task));

        Iterable<Task> actualResult = tasksService.filterTasks(
                taskList,
                new Date(2022, Calendar.MARCH, 3, 17, 0),
                new Date(2022, Calendar.MARCH, 4, 17, 0)
        );

        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("F02_TC05")
    void filterTasks_ListContainingExecutingAtEnd_ReturnsListContainingTheMatchingTask() {
        Task task = new Task(
                "task executing at end",
                new Date(2022, Calendar.MAY, 15, 17, 0)
        );
        task.setActive(true);
        taskList.add(task);

        Iterable<Task> expectedResult = new ArrayList<>(Collections.singletonList(task));

        Iterable<Task> actualResult = tasksService.filterTasks(
                taskList,
                new Date(2022, Calendar.MARCH, 3, 17, 0),
                new Date(2022, Calendar.MAY, 15, 17, 0)
        );

        assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("F02_TC06")
    void filterTasks_ListContainingExecutingAfterEnd_ReturnsEmptyList() {
        Task task = new Task(
                "task executing after end",
                new Date(2022, Calendar.MARCH, 5, 19, 0)
        );
        task.setActive(true);
        taskList.add(task);

        Iterable<Task> expectedResult = new ArrayList<>();

        Iterable<Task> actualResult = tasksService.filterTasks(
                taskList,
                new Date(2022, Calendar.MARCH, 3, 17, 0),
                new Date(2022, Calendar.MARCH, 4, 17, 0)
        );

        assertEquals(expectedResult, actualResult);
    }
}