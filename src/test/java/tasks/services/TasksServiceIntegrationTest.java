package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.model.validator.TaskValidator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TasksServiceIntegrationTest {

    private TasksService tasksService;
    private ObservableList<Task> taskList;

    @BeforeEach
    void setUp() {
        tasksService = new TasksService(new ArrayTaskList(), new TaskValidator());
        taskList = FXCollections.observableList(new ArrayList<>());
    }

    @Test
    @Tag("INT")
    void add_ValidTask_ReturnsAddedTask() {
        int taskListSizeBefore = taskList.size();
        Task validTask = new Task(
                "titlu de lungime valida",
                new Date(2022, Calendar.MARCH, 13, 17, 0),
                new Date(2022, Calendar.MARCH, 13, 18, 0),
                120
        );

        Task returnedTask = tasksService.add(validTask, taskList);

        assertEquals(returnedTask, validTask);
        assertEquals(taskList.size(), taskListSizeBefore + 1);
    }

    @Test
    @Tag("INT")
    @DisplayName("TC_01_Int_Step3")
    void add_InvalidTask_ThrowsIllegalArgumentException() {
        int taskListSizeBefore = taskList.size();
        Task invalidTask = new Task(
                "abcd",
                new Date(2022, Calendar.MARCH, 13, 17, 0),
                new Date(2022, Calendar.MARCH, 13, 18, 0),
                120
        );

        assertThrows(IllegalArgumentException.class, () -> tasksService.add(
                invalidTask,
                taskList
        ));

        assertEquals(taskList.size(), taskListSizeBefore);
    }
}
