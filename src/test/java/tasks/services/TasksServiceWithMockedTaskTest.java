package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.model.validator.TaskValidator;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TasksServiceWithMockedTaskTest {

    private TasksService tasksService;
    private ObservableList<Task> taskList;

    @BeforeEach
    void setUp() {
        tasksService = new TasksService(new ArrayTaskList(), new TaskValidator());
        taskList = FXCollections.observableList(new ArrayList<>());
    }

    @Test
    @Tag("INT")
    @DisplayName("TC_01_Int_Step2")
    void add_ValidTask_ReturnsAddedTask() {
        int taskListSizeBefore = taskList.size();
        Task validTask = mock(Task.class);

        Mockito.when(validTask.isRepeated()).thenReturn(false);
        Mockito.when(validTask.getTitle()).thenReturn("valid title");
        Mockito.when(validTask.getStartTime()).thenReturn(new Date(2022, Calendar.MARCH, 13, 17, 0));

        Task returnedTask = tasksService.add(validTask, taskList);

        assertEquals(returnedTask, validTask);
        assertEquals(taskList.size(), taskListSizeBefore + 1);
    }

    @Test
    @Tag("INT")
    @DisplayName("TC02-MT")
    void add_InvalidTask_ThrowsIllegalArgumentException() {
        int taskListSizeBefore = taskList.size();
        Task invalidTask = mock(Task.class);

        Mockito.when(invalidTask.isRepeated()).thenReturn(false);
        Mockito.when(invalidTask.getTitle()).thenReturn("abc");
        Mockito.when(invalidTask.getStartTime()).thenReturn(new Date(2022, Calendar.MARCH, 13, 17, 0));

        assertThrows(IllegalArgumentException.class, () -> tasksService.add(
                invalidTask,
                taskList
        ));

        assertEquals(taskList.size(), taskListSizeBefore);
    }
}
