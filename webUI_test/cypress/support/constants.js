export const Constants = {
    BASE_URL : "https://petstore.octoperf.com",
    HOME_PATH : "/actions/Catalog.action",
    LOGIN_PATH : "/actions/Account.action",
    CART_PATH : "/actions/Cart.action?viewCart=",
    CART_OPERATIONS_PATH: "/actions/Cart.action"
}