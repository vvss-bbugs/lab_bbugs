// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import { Constants } from './constants';

Cypress.Commands.add('login', (username, password) => {
        cy.get("input[name=\"username\"]")
            .type(username);
        cy.get("input[name=\"password\"]")
            .type("{backspace}{backspace}{backspace}{backspace}")
            .type(password);
        cy.get("input[name=\"signon\"]")
            .click();
});

Cypress.Commands.add('addToCart', (product) => {
        cy.visit(Constants.BASE_URL+Constants.HOME_PATH);
        
        cy.get('#SidebarContent > [href="/actions/Catalog.action'
                        //+';jsessionid='+ sid.value
                        +'?viewCategory=&categoryId='
                        + product.category.toUpperCase()
                        + '"] > img'
                ).click();

        cy.contains(product.productId)
                .click();

        cy.contains(product.itemId)
                .click();

        cy.contains('Add to Cart')
                .click();
        
        cy.contains(product.productId);
        cy.contains(product.itemId);

});

Cypress.Commands.add('updateProductInCart', (product) => {
        cy.get('input[name="'+product.itemId+'"]')
                .type("{backspace}"+product.newQuantity);
        cy.get('input[name=updateCartQuantities]')
                .click();
        cy.contains(product.itemId)
                .parent()
                .siblings()
                .contains("$"+product.price*product.newQuantity);
        
});

Cypress.Commands.add('deleteAllCartItems', () => {
        cy.visit(Constants.BASE_URL+Constants.CART_PATH); 
        
        cy.get('body')
                .then(($body) => {
                        if ($body.text().includes('Remove')) {
                                var noItems = 0;
                                cy.get('.Button').then( ($buttons) => {
                                        
                                        for(let i=0; i< $buttons.length; i++){
                                                if($buttons[i].innerText == "Remove"){
                                                        noItems++;
                                                }
                                        }
                                        
                                        for(let i=0; i<noItems; i++){
                                                cy.contains("Remove")
                                                        .click();
                                        }

                                });
                        }
        });
});
