import { Constants } from '../support/constants';

describe('Testing Login Functionality', () => {
    var loginData;

    before(() => {
        cy.fixture('login').then((data) => {
            loginData = data;
        });
    });

    beforeEach( () => {
        cy.visit(Constants.BASE_URL + Constants.LOGIN_PATH);
    });

    it('Test Valid Login', () => {
        let loginValidCredentials = loginData.validCredentials;
        cy.login(
            loginValidCredentials.username, 
            loginValidCredentials.password
        );

        cy.location('pathname')
            .should('eq', Constants.HOME_PATH);

        cy.get('#Banner > img');

        cy.contains('a', 'Sign Out')
            .click();
        
        cy.contains('a', 'Sign In');
    });

    it('Test Invalid Login', () => {
        let loginInvalidCredentials = loginData.invalidCredentials;
        cy.login(
            loginInvalidCredentials.username, 
            loginInvalidCredentials.password
        );

        cy.location('pathname')
            .should('contain', Constants.LOGIN_PATH);

        cy.contains('ul > li', loginData.invalidLoginMessage);
    });
});