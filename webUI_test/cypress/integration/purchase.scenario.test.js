import { Constants } from '../support/constants';

describe('Testing Login -> Add to Cart -> Update Cart -> Checkout -> Logout Scenario', () => {
    var purchaseData;
    var total;
    var defaultQuantity = 1;

    const login = () => {
        cy.visit(Constants.BASE_URL + Constants.LOGIN_PATH);
        cy.fixture('login').then((data) => {
            cy.login(
                data.validCredentials.username,
                data.validCredentials.password
            );
        });
    }

    before(() => {
       cy.fixture('purchase').then((data) => {
            purchaseData = data;
        });
        total = 0;
        login();
        cy.deleteAllCartItems();
    });

    beforeEach(() => {
    // otherwise Cypress clears cookies after each test
    Cypress.Cookies.preserveOnce('JSESSIONID');
  })

    it('Test Valid Login', ()=>{
        login();
    });

    it('Test Add to Cart', ()=> {
        
        purchaseData.productsToAdd.forEach( p => {
            cy.addToCart(p);
            total += p.price;
        });

        cy.contains('Sub Total: $' + total);
    });

    it('Test Update Cart', ()=> {
        
        purchaseData.productsToUpdate.forEach( p => {
            cy.updateProductInCart(p);
            total += p.price * (p.newQuantity - defaultQuantity);
        });

        cy.contains('Sub Total: $' + total);
    });

    it('Test Checkout', () => {
        cy.contains('Proceed to Checkout')
            .click();

        cy.get('select[name="order.cardType"]')
            .should('have.value', purchaseData.checkoutData.cardType);
        cy.get('input[name="order.creditCard"]')
            .should('have.value', purchaseData.checkoutData.cardNumber);
        cy.get('input[name="order.expiryDate"]')
            .should('have.value', purchaseData.checkoutData.expiryDate);
        cy.get('input[name="order.billToFirstName"]')
            .should('have.value', purchaseData.checkoutData.firstName);
        cy.get('input[name="order.billToLastName"]')
            .should('have.value', purchaseData.checkoutData.lastName);
        cy.get('input[name="order.billAddress1"]')
            .should('have.value', purchaseData.checkoutData.address);

        cy.contains('Continue')
            .click();

        cy.get('tr:nth-child(3) td:nth-child(2)')
            .contains(purchaseData.checkoutData.firstName);
        cy.get('tr:nth-child(4) td:nth-child(2)')
            .contains(purchaseData.checkoutData.lastName);
        cy.get('tr:nth-child(5) td:nth-child(2)')
            .contains(purchaseData.checkoutData.address);
        cy.get('tr:nth-child(12) td:nth-child(2)')
            .contains(purchaseData.checkoutData.firstName);
        cy.get('tr:nth-child(13) td:nth-child(2)')
            .contains(purchaseData.checkoutData.lastName);
        cy.get('tr:nth-child(14) td:nth-child(2)')
            .contains(purchaseData.checkoutData.address);

        cy.contains('Confirm')
            .click();

        cy.get('td > table').contains('Total: $' + total);
    });

    it('Test Logout', () => {
        cy.get('a[href="/actions/Account.action?signoff="]')
            .click();

        cy.location('pathname')
            .should('eq', Constants.HOME_PATH);
        cy.get('#Menu').contains('Sign In');
    })

});