import { Constants } from "../support/constants";

describe('Testing Update Cart Functionality', () => {

    var data;

    before(() => {
       cy.fixture('update-cart').then(testData => {
           data = testData;

           cy.visit(Constants.BASE_URL + Constants.LOGIN_PATH);
           cy.login(data.loginCredentials.username, data.loginCredentials.password);
       });
    });

    beforeEach( () => {
        cy.deleteAllCartItems();
        cy.addToCart(data.productToAdd);
    });

    it('Test Valid Update Cart', () => {
        cy.location('pathname')
            .should('eq', Constants.CART_OPERATIONS_PATH);

        cy.get('input[name="'+data.productsToUpdate.validProductToUpdate.itemId+'"]')
            .type("{backspace}"+data.productsToUpdate.validProductToUpdate.newQuantity);
        cy.get('input[name=updateCartQuantities]')
            .click();

        cy.get('input[name="'+data.productsToUpdate.validProductToUpdate.itemId+'"]')
            .should('have.value', data.productsToUpdate.validProductToUpdate.newQuantity);
        cy.get('tr:nth-child(2) td:nth-child(7)')
            .contains('td', '$' + data.productsToUpdate.validProductToUpdate.newQuantity *
                data.productsToUpdate.validProductToUpdate.price);
    });

    it('Test Invalid Update Cart', () => {
        const quantityBefore = Cypress
            .$('input[name="'+data.productsToUpdate.invalidProductToUpdate.itemId+'"]').val();
        const totalPriceBefore = Cypress
            .$('tr:nth-child(2) td:nth-child(7)').html();

        cy.location('pathname')
            .should('eq', Constants.CART_OPERATIONS_PATH);

        cy.get('input[name="'+data.productsToUpdate.invalidProductToUpdate.itemId+'"]')
            .type("{backspace}"+data.productsToUpdate.invalidProductToUpdate.newQuantity);
        cy.get('input[name=updateCartQuantities]')
            .click();

        cy.get('input[name="'+data.productsToUpdate.validProductToUpdate.itemId+'"]')
            .should('have.value', quantityBefore);
        cy.get('tr:nth-child(2) td:nth-child(7)').contains('td', totalPriceBefore);
    });
})