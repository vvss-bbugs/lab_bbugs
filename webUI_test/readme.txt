generare raport
-----------------------------------------------------------------------
1. rulare teste din terminal
    npx cypress run
2. merge json-uri cu rezultatele spec-urilor intr-un singur fisier
    npx mochawesome-merge "./cypress/results/*.json" > mochawesome.json
3. encode fisier rezultat (altfel nu merge marge)
    node encode.js
4. generare html
    npx marge mochawesome.json
=> html in mochawesome-report